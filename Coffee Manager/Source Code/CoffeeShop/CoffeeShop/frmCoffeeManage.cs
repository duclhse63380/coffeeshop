﻿
using CoffeeShop.DAO;
using CoffeeShop.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Menu = CoffeeShop.DTO.Menu;

namespace CoffeeShop
{
    public partial class frmCoffeeManage : Form
    {
        private Account loginAccount;
        
        public Account LoginAccount
        {
            get { return loginAccount; }
            set { loginAccount = value; ChangeAccount(loginAccount.Type); }
        }
        public frmCoffeeManage(Account acco)
        {
            InitializeComponent();
            this.LoginAccount = acco;
            LoadTable();
            LoadCategory();
           
        }
        void ChangeAccount(int type)
        {
            adminToolStripMenuItem.Enabled = type == 1;
            txtDisplayName.Text = loginAccount.DisplayName;
        }
        // load danh sách food lên combobox
        void LoadFoodListByCategoryID(int id)
        {
            List<Food> listFood = FoodDAO.Instance.GetFoodByCategory(id);
            cbFood.DataSource = listFood;
            cbFood.DisplayMember = "Name";
        }
        // load danh sách category lên combobox
        void LoadCategory()
        {
            List<Category> listCategory = CategoryDAO.Instance.GetListCategory();
            cbCategory.DataSource = listCategory;
            cbCategory.DisplayMember = "Name";
        }

        // Load Bàn 
        void LoadTable()
        {
            flpTable.Controls.Clear();
            List<Table>  listTable =TableDAO.Instance.LoadListTable();
            foreach (Table item in listTable)
            {
                Button btn = new Button() {
                    Width = TableDAO.TableWidth,
                    Height = TableDAO.TableHeight
                };
                btn.Text = item.Name+ Environment.NewLine +"( "+ item.Status+ " )";
                btn.Click += btn_Click;
                btn.Tag = item; 
                flpTable.Controls.Add(btn);
                if (item.Status == "Trống")
                {
                    btn.BackColor = Color.Aqua;
                }
                else
                    btn.BackColor = Color.LightYellow;
            }

        }
    
      // click show billInfo theo bàn 
        void btn_Click(object sender, EventArgs e)
        {
            int tableID = ((sender as Button).Tag as Table).ID;
            lsvBill.Tag = (sender as Button).Tag;

            txtTable.Text = ((sender as Button).Tag as Table).Name;

            ShowBill(tableID);

        }
        // show infoBill
        void ShowBill(int id)
        {
            lsvBill.Items.Clear();
            List <Menu> listBillInfo = LoadMenuDAO.Instance.GetListMenuByTable(id);
            float totalPrice = 0;
            foreach (Menu item in listBillInfo)
            {
                ListViewItem lsvItem = new ListViewItem(item.FoodName.ToString());
                lsvItem.SubItems.Add(item.Count.ToString());
                lsvItem.SubItems.Add(item.Price.ToString());
                lsvItem.SubItems.Add(item.TotalPrice.ToString());
                totalPrice += item.TotalPrice;
                lsvBill.Items.Add(lsvItem);
            }
           
            txtTotalPrice.Text = totalPrice.ToString();
           
        }
        

        

      
        // khi insertFood bên trang con thì bên trang thanh toán sẽ tự động update theo giá trị thay đổi 
        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAdmin f = new frmAdmin();
            f.InsertFood += f_InsertFood;
            f.UpdateFood += f_UpdateFood;
            f.DeleteFood += f_DeleteFood;
            f.ShowDialog();
        }

        private void f_DeleteFood(object sender, EventArgs e)
        {
            LoadFoodListByCategoryID((cbCategory.SelectedItem as Category).ID);
            if (lsvBill.Tag != null)
                ShowBill((lsvBill.Tag as Table).ID);
            LoadTable();
        }

        private void f_InsertFood(object sender, EventArgs e)
        {
            LoadFoodListByCategoryID((cbCategory.SelectedItem as Category).ID);
            if (lsvBill.Tag != null)
      
            ShowBill((lsvBill.Tag as Table).ID);
        }

        private void f_UpdateFood(object sender, EventArgs e)
        {
           
            LoadFoodListByCategoryID((cbCategory.SelectedItem as Category).ID);
            if (lsvBill.Tag != null)
                ShowBill((lsvBill.Tag as Table).ID);
        }

      

        
        
        // lấy dữ liệu đồ uống theo category
        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = 0;
            ComboBox cb = sender as ComboBox;
            if (cb.SelectedItem == null)
            {
                return;
            }
            Category selected = cb.SelectedItem as Category;
            id = selected.ID;
            LoadFoodListByCategoryID(id);
        }

        // Order đồ uống 
        private void btnAddFood_Click(object sender, EventArgs e)
        {
            
            Table table = lsvBill.Tag as Table;
            if (table == null)
            {
                MessageBox.Show("Please Click Table");
                return;
            }
            int idBill = BillDAO.Instance.GetUncheckBillIdbyTableId(table.ID);
            int foodID = (cbFood.SelectedItem as Food).ID;
            int count = (int)nmFoodCount.Value;
            string displayName = txtDisplayName.Text;

            if (idBill == -1)
            {
                BillDAO.Instance.InsertBill(table.ID,displayName);
                BillInfoDAO.Instance.InsertBillInfo(BillDAO.Instance.getMaxIdBill(),foodID,count);

            }
            else
            {
                BillInfoDAO.Instance.InsertBillInfo(idBill,foodID, count);

            }
            ShowBill(table.ID);
            nmFoodCount.Value = 1;
            LoadTable();



        }

        private void cbFood_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // CheckOut
        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            Table table = lsvBill.Tag as Table;
            if (table == null)
            {
                MessageBox.Show("Hãy chọn bàn");
                return;
            }
            int idBill = BillDAO.Instance.GetUncheckBillIdbyTableId(table.ID);
            int discount = (int)nmDiscount.Value;
            double totalPrice = Convert.ToDouble(txtTotalPrice.Text);
            double finalTotalPrice = totalPrice - (totalPrice / 100) * discount;
            
            if (table.Status == "Trống")
            {
                MessageBox.Show("Bàn hiện tại trống , không thể thanh toán !");
            }
            else if (idBill != -1) 
            {
               
                if (MessageBox.Show("Bạn muốn thanh toán "+ table.Name+"?\n" + "Tổng Tiền: " + totalPrice +" đồng"+"\n" +"Giảm giá: "+ discount +" %"+"\n"+"Thành Tiền: " + finalTotalPrice + " đồng", "Notify",MessageBoxButtons.OKCancel) == DialogResult.OK) 
                {
                    BillDAO.Instance.checkOut(idBill, discount,(float)finalTotalPrice);
                }
                nmDiscount.Value = 0;
                txtPhoneMember.Text = "";
                ShowBill(table.ID);
                LoadTable();
              
            }
        }
        void CheckMemberPhone()
        {
            string phoneMember = txtPhoneMember.Text;
            List<Member> listMember = MemberDAO.Instance.GetListMember(phoneMember);
            foreach (Member item in listMember)
            {
                
                if (txtPhoneMember.Text.Equals(item.Phone))
                {
                    string phone = txtPhoneMember.Text;
                    float point = float.Parse(txtTotalPrice.Text);
                    MemberPointsDAO.Instance.InsertMemberPoints(phone,point);
                    MessageBox.Show("SuccessFull");
                    showPoints();
                }
                else
     
                    MessageBox.Show("Member does not exist");

            }         
        }
       
        void showPoints()
        {
            string phoneMember = txtPhoneMember.Text;
            List<MemberPoints> listMember = MemberPointsDAO.Instance.GetListMemberPoints(phoneMember);
            float totalPoint = 0;
            foreach (MemberPoints item in listMember)
            {
                totalPoint += item.Point;
                MemberDAO.Instance.UpdateMember(totalPoint , phoneMember);
                if ( totalPoint > 7000000)
                {
                    nmDiscount.Value = 20;
                }
                else if (totalPoint < 2000000)
                {
                    nmDiscount.Value = 5;
                }
                else if (totalPoint > 2000000 || totalPoint < 7000000)
                {
                    nmDiscount.Value = 10;
                }

            }


        }

        private void btnCheckPhone_Click(object sender, EventArgs e)
        {
            if (CheckInput())
            {
                CheckMemberPhone();
            }
            else
            {
                MessageBox.Show("Please fill all text box!");
            }
           
        }
        private bool CheckInput()
        {
            string phone = txtPhoneMember.Text.Trim();

            if (phone.Length == 0)
            {
                return false;
            }
            return true;
        }
        private void registerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRegisterMember member = new frmRegisterMember();
            member.ShowDialog();
        }

        private void memberProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MemberProfile profile = new MemberProfile();
            profile.ShowDialog();
        }
        private void thanhToánToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnCheckOut_Click(this, new EventArgs());
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void oderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnAddFood_Click(this, new EventArgs());
        }
        private void showListBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBillTotal billTotal = new frmBillTotal();
            billTotal.ShowDialog();
        }
        private void btnLongExit_Click(object sender, EventArgs e)
        {

            this.Close();


        }

        private void infomationAccoutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            frmAccountProfile f = new frmAccountProfile(LoginAccount);
            f.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void frmCoffeeManage_Click(object sender, EventArgs e)
        {

        }

        private void frmCoffeeManage_MouseClick(object sender, MouseEventArgs e)
        {

        }

       
        private void memberRegisterToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void phímTắtToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void accountToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }

        private void infomationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
