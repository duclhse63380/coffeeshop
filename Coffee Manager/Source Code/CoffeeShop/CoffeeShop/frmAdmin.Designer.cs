﻿namespace CoffeeShop
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdmin));
            this.tcAccount = new System.Windows.Forms.TabPage();
            this.panel45 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtSearchAccount = new System.Windows.Forms.TextBox();
            this.btnFindAccount = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.btnLoadAccount = new System.Windows.Forms.Button();
            this.btnDeleteAccount = new System.Windows.Forms.Button();
            this.btnUpdateAccount = new System.Windows.Forms.Button();
            this.btnAddAccount = new System.Windows.Forms.Button();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.nmTypeAccount = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.txtDisplayNameAccount = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.txtUserNameAccount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtgvViewAccount = new System.Windows.Forms.DataGridView();
            this.tcTable = new System.Windows.Forms.TabPage();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.cbStatusTable = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.btnViewTable = new System.Windows.Forms.Button();
            this.btnDeleteTable = new System.Windows.Forms.Button();
            this.btnUpdateTable = new System.Windows.Forms.Button();
            this.btnAddTable = new System.Windows.Forms.Button();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.txtNameTable = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.txtIDTable = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtgvViewTable = new System.Windows.Forms.DataGridView();
            this.tcCategory = new System.Windows.Forms.TabPage();
            this.panel26 = new System.Windows.Forms.Panel();
            this.txtSearchCategory = new System.Windows.Forms.TextBox();
            this.btnSearchCategory = new System.Windows.Forms.Button();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.txtNameCategory = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.btnViewCategory = new System.Windows.Forms.Button();
            this.btnDeleteCategory = new System.Windows.Forms.Button();
            this.btnUpdateCategory = new System.Windows.Forms.Button();
            this.btnAddCategory = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.txtIdCategory = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtgvViewCategory = new System.Windows.Forms.DataGridView();
            this.tcFood = new System.Windows.Forms.TabPage();
            this.dtgvFood = new System.Windows.Forms.DataGridView();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.nmFoodPrice = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.lbCategory = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnViewFood = new System.Windows.Forms.Button();
            this.btnDeleteFood = new System.Windows.Forms.Button();
            this.btnUpdateFood = new System.Windows.Forms.Button();
            this.btnAddFood = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.txtFoodName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.txtFoodID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtFindFood = new System.Windows.Forms.TextBox();
            this.btnFindFood = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.tcAdmin = new System.Windows.Forms.TabControl();
            this.btnLongExit = new System.Windows.Forms.Button();
            this.tcAccount.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel48.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmTypeAccount)).BeginInit();
            this.panel46.SuspendLayout();
            this.panel47.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvViewAccount)).BeginInit();
            this.tcTable.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvViewTable)).BeginInit();
            this.tcCategory.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvViewCategory)).BeginInit();
            this.tcFood.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvFood)).BeginInit();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFoodPrice)).BeginInit();
            this.panel19.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tcAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcAccount
            // 
            this.tcAccount.BackColor = System.Drawing.Color.Snow;
            this.tcAccount.Controls.Add(this.panel45);
            this.tcAccount.Controls.Add(this.dtgvViewAccount);
            this.tcAccount.Location = new System.Drawing.Point(4, 25);
            this.tcAccount.Name = "tcAccount";
            this.tcAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tcAccount.Size = new System.Drawing.Size(988, 624);
            this.tcAccount.TabIndex = 4;
            this.tcAccount.Text = "Account";
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.SeaShell;
            this.panel45.Controls.Add(this.panel3);
            this.panel45.Controls.Add(this.panel23);
            this.panel45.Controls.Add(this.panel48);
            this.panel45.Controls.Add(this.panel46);
            this.panel45.Controls.Add(this.panel47);
            this.panel45.Location = new System.Drawing.Point(548, 49);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(437, 544);
            this.panel45.TabIndex = 11;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtSearchAccount);
            this.panel3.Controls.Add(this.btnFindAccount);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Location = new System.Drawing.Point(17, 395);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(403, 118);
            this.panel3.TabIndex = 13;
            // 
            // txtSearchAccount
            // 
            this.txtSearchAccount.Location = new System.Drawing.Point(39, 45);
            this.txtSearchAccount.Name = "txtSearchAccount";
            this.txtSearchAccount.Size = new System.Drawing.Size(218, 22);
            this.txtSearchAccount.TabIndex = 9;
            // 
            // btnFindAccount
            // 
            this.btnFindAccount.Location = new System.Drawing.Point(308, 31);
            this.btnFindAccount.Name = "btnFindAccount";
            this.btnFindAccount.Size = new System.Drawing.Size(95, 50);
            this.btnFindAccount.TabIndex = 8;
            this.btnFindAccount.Text = "Find";
            this.btnFindAccount.UseVisualStyleBackColor = true;
            this.btnFindAccount.Click += new System.EventHandler(this.btnFindAccount_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(556, 17);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(420, 118);
            this.panel4.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(588, 14);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(398, 118);
            this.panel5.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(588, 14);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(398, 118);
            this.panel6.TabIndex = 2;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.btnLoadAccount);
            this.panel23.Controls.Add(this.btnDeleteAccount);
            this.panel23.Controls.Add(this.btnUpdateAccount);
            this.panel23.Controls.Add(this.btnAddAccount);
            this.panel23.Controls.Add(this.panel34);
            this.panel23.Controls.Add(this.panel44);
            this.panel23.Location = new System.Drawing.Point(21, 221);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(399, 168);
            this.panel23.TabIndex = 12;
            // 
            // btnLoadAccount
            // 
            this.btnLoadAccount.Location = new System.Drawing.Point(233, 99);
            this.btnLoadAccount.Name = "btnLoadAccount";
            this.btnLoadAccount.Size = new System.Drawing.Size(95, 50);
            this.btnLoadAccount.TabIndex = 7;
            this.btnLoadAccount.Text = "xem";
            this.btnLoadAccount.UseVisualStyleBackColor = true;
            this.btnLoadAccount.Click += new System.EventHandler(this.btnLoadAccount_Click);
            // 
            // btnDeleteAccount
            // 
            this.btnDeleteAccount.Location = new System.Drawing.Point(233, 31);
            this.btnDeleteAccount.Name = "btnDeleteAccount";
            this.btnDeleteAccount.Size = new System.Drawing.Size(95, 50);
            this.btnDeleteAccount.TabIndex = 6;
            this.btnDeleteAccount.Text = "xóa";
            this.btnDeleteAccount.UseVisualStyleBackColor = true;
            this.btnDeleteAccount.Click += new System.EventHandler(this.btnDeleteAccount_Click);
            // 
            // btnUpdateAccount
            // 
            this.btnUpdateAccount.Location = new System.Drawing.Point(63, 99);
            this.btnUpdateAccount.Name = "btnUpdateAccount";
            this.btnUpdateAccount.Size = new System.Drawing.Size(95, 50);
            this.btnUpdateAccount.TabIndex = 5;
            this.btnUpdateAccount.Text = "Sửa";
            this.btnUpdateAccount.UseVisualStyleBackColor = true;
            this.btnUpdateAccount.Click += new System.EventHandler(this.btnUpdateAccount_Click);
            // 
            // btnAddAccount
            // 
            this.btnAddAccount.Location = new System.Drawing.Point(63, 31);
            this.btnAddAccount.Name = "btnAddAccount";
            this.btnAddAccount.Size = new System.Drawing.Size(95, 50);
            this.btnAddAccount.TabIndex = 4;
            this.btnAddAccount.Text = "Thêm";
            this.btnAddAccount.UseVisualStyleBackColor = true;
            this.btnAddAccount.Click += new System.EventHandler(this.btnAddAccount_Click_1);
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.panel35);
            this.panel34.Location = new System.Drawing.Point(556, 17);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(420, 118);
            this.panel34.TabIndex = 3;
            // 
            // panel35
            // 
            this.panel35.Location = new System.Drawing.Point(588, 14);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(398, 118);
            this.panel35.TabIndex = 2;
            // 
            // panel44
            // 
            this.panel44.Location = new System.Drawing.Point(588, 14);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(398, 118);
            this.panel44.TabIndex = 2;
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.nmTypeAccount);
            this.panel48.Controls.Add(this.label11);
            this.panel48.Location = new System.Drawing.Point(4, 142);
            this.panel48.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(431, 62);
            this.panel48.TabIndex = 4;
            // 
            // nmTypeAccount
            // 
            this.nmTypeAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmTypeAccount.Location = new System.Drawing.Point(225, 20);
            this.nmTypeAccount.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmTypeAccount.Name = "nmTypeAccount";
            this.nmTypeAccount.Size = new System.Drawing.Size(120, 24);
            this.nmTypeAccount.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(13, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 24);
            this.label11.TabIndex = 0;
            this.label11.Text = "Title Account";
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.txtDisplayNameAccount);
            this.panel46.Controls.Add(this.label9);
            this.panel46.Location = new System.Drawing.Point(3, 76);
            this.panel46.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(431, 62);
            this.panel46.TabIndex = 3;
            // 
            // txtDisplayNameAccount
            // 
            this.txtDisplayNameAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplayNameAccount.Location = new System.Drawing.Point(150, 17);
            this.txtDisplayNameAccount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDisplayNameAccount.Name = "txtDisplayNameAccount";
            this.txtDisplayNameAccount.Size = new System.Drawing.Size(278, 28);
            this.txtDisplayNameAccount.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 24);
            this.label9.TabIndex = 0;
            this.label9.Text = "Display Name";
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.txtUserNameAccount);
            this.panel47.Controls.Add(this.label10);
            this.panel47.Location = new System.Drawing.Point(3, 10);
            this.panel47.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(431, 62);
            this.panel47.TabIndex = 2;
            // 
            // txtUserNameAccount
            // 
            this.txtUserNameAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserNameAccount.Location = new System.Drawing.Point(150, 17);
            this.txtUserNameAccount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUserNameAccount.Name = "txtUserNameAccount";
            this.txtUserNameAccount.Size = new System.Drawing.Size(278, 28);
            this.txtUserNameAccount.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(32, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 24);
            this.label10.TabIndex = 0;
            this.label10.Text = "User Name";
            // 
            // dtgvViewAccount
            // 
            this.dtgvViewAccount.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvViewAccount.BackgroundColor = System.Drawing.Color.SeaShell;
            this.dtgvViewAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvViewAccount.GridColor = System.Drawing.Color.Black;
            this.dtgvViewAccount.Location = new System.Drawing.Point(3, 49);
            this.dtgvViewAccount.Name = "dtgvViewAccount";
            this.dtgvViewAccount.RowHeadersWidth = 51;
            this.dtgvViewAccount.RowTemplate.Height = 24;
            this.dtgvViewAccount.Size = new System.Drawing.Size(536, 544);
            this.dtgvViewAccount.TabIndex = 10;
            // 
            // tcTable
            // 
            this.tcTable.BackColor = System.Drawing.Color.Snow;
            this.tcTable.Controls.Add(this.panel40);
            this.tcTable.Controls.Add(this.dtgvViewTable);
            this.tcTable.Location = new System.Drawing.Point(4, 25);
            this.tcTable.Name = "tcTable";
            this.tcTable.Padding = new System.Windows.Forms.Padding(3);
            this.tcTable.Size = new System.Drawing.Size(988, 624);
            this.tcTable.TabIndex = 2;
            this.tcTable.Text = "Table";
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.SeaShell;
            this.panel40.Controls.Add(this.panel43);
            this.panel40.Controls.Add(this.panel36);
            this.panel40.Controls.Add(this.panel41);
            this.panel40.Controls.Add(this.panel42);
            this.panel40.Location = new System.Drawing.Point(545, 60);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(437, 523);
            this.panel40.TabIndex = 11;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.cbStatusTable);
            this.panel43.Controls.Add(this.label8);
            this.panel43.Location = new System.Drawing.Point(0, 142);
            this.panel43.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(431, 62);
            this.panel43.TabIndex = 4;
            this.panel43.Paint += new System.Windows.Forms.PaintEventHandler(this.panel43_Paint);
            // 
            // cbStatusTable
            // 
            this.cbStatusTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbStatusTable.FormattingEnabled = true;
            this.cbStatusTable.Items.AddRange(new object[] {
            "Trống",
            "Có Người"});
            this.cbStatusTable.Location = new System.Drawing.Point(153, 17);
            this.cbStatusTable.Name = "cbStatusTable";
            this.cbStatusTable.Size = new System.Drawing.Size(275, 30);
            this.cbStatusTable.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "Status";
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.btnViewTable);
            this.panel36.Controls.Add(this.btnDeleteTable);
            this.panel36.Controls.Add(this.btnUpdateTable);
            this.panel36.Controls.Add(this.btnAddTable);
            this.panel36.Controls.Add(this.panel37);
            this.panel36.Controls.Add(this.panel39);
            this.panel36.Location = new System.Drawing.Point(69, 237);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(299, 201);
            this.panel36.TabIndex = 12;
            // 
            // btnViewTable
            // 
            this.btnViewTable.Location = new System.Drawing.Point(181, 138);
            this.btnViewTable.Name = "btnViewTable";
            this.btnViewTable.Size = new System.Drawing.Size(95, 50);
            this.btnViewTable.TabIndex = 7;
            this.btnViewTable.Text = "xem";
            this.btnViewTable.UseVisualStyleBackColor = true;
            this.btnViewTable.Click += new System.EventHandler(this.btnViewTable_Click);
            // 
            // btnDeleteTable
            // 
            this.btnDeleteTable.Location = new System.Drawing.Point(181, 17);
            this.btnDeleteTable.Name = "btnDeleteTable";
            this.btnDeleteTable.Size = new System.Drawing.Size(95, 50);
            this.btnDeleteTable.TabIndex = 6;
            this.btnDeleteTable.Text = "xóa";
            this.btnDeleteTable.UseVisualStyleBackColor = true;
            this.btnDeleteTable.Click += new System.EventHandler(this.btnDeleteTable_Click);
            // 
            // btnUpdateTable
            // 
            this.btnUpdateTable.Location = new System.Drawing.Point(13, 138);
            this.btnUpdateTable.Name = "btnUpdateTable";
            this.btnUpdateTable.Size = new System.Drawing.Size(95, 50);
            this.btnUpdateTable.TabIndex = 5;
            this.btnUpdateTable.Text = "Sửa";
            this.btnUpdateTable.UseVisualStyleBackColor = true;
            this.btnUpdateTable.Click += new System.EventHandler(this.btnUpdateTable_Click);
            // 
            // btnAddTable
            // 
            this.btnAddTable.Location = new System.Drawing.Point(13, 17);
            this.btnAddTable.Name = "btnAddTable";
            this.btnAddTable.Size = new System.Drawing.Size(95, 50);
            this.btnAddTable.TabIndex = 4;
            this.btnAddTable.Text = "Thêm";
            this.btnAddTable.UseVisualStyleBackColor = true;
            this.btnAddTable.Click += new System.EventHandler(this.btnAddTable_Click);
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.panel38);
            this.panel37.Location = new System.Drawing.Point(556, 17);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(420, 118);
            this.panel37.TabIndex = 3;
            // 
            // panel38
            // 
            this.panel38.Location = new System.Drawing.Point(588, 14);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(398, 118);
            this.panel38.TabIndex = 2;
            // 
            // panel39
            // 
            this.panel39.Location = new System.Drawing.Point(588, 14);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(398, 118);
            this.panel39.TabIndex = 2;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.txtNameTable);
            this.panel41.Controls.Add(this.label4);
            this.panel41.Location = new System.Drawing.Point(3, 76);
            this.panel41.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(431, 62);
            this.panel41.TabIndex = 3;
            // 
            // txtNameTable
            // 
            this.txtNameTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameTable.Location = new System.Drawing.Point(150, 17);
            this.txtNameTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNameTable.Name = "txtNameTable";
            this.txtNameTable.Size = new System.Drawing.Size(278, 28);
            this.txtNameTable.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Table Name";
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.txtIDTable);
            this.panel42.Controls.Add(this.label5);
            this.panel42.Location = new System.Drawing.Point(3, 10);
            this.panel42.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(431, 62);
            this.panel42.TabIndex = 2;
            // 
            // txtIDTable
            // 
            this.txtIDTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDTable.Location = new System.Drawing.Point(150, 17);
            this.txtIDTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtIDTable.Name = "txtIDTable";
            this.txtIDTable.ReadOnly = true;
            this.txtIDTable.Size = new System.Drawing.Size(278, 28);
            this.txtIDTable.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(48, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "ID";
            // 
            // dtgvViewTable
            // 
            this.dtgvViewTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvViewTable.BackgroundColor = System.Drawing.Color.SeaShell;
            this.dtgvViewTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvViewTable.Location = new System.Drawing.Point(3, 19);
            this.dtgvViewTable.Name = "dtgvViewTable";
            this.dtgvViewTable.RowHeadersWidth = 51;
            this.dtgvViewTable.RowTemplate.Height = 24;
            this.dtgvViewTable.Size = new System.Drawing.Size(536, 613);
            this.dtgvViewTable.TabIndex = 10;
            // 
            // tcCategory
            // 
            this.tcCategory.BackColor = System.Drawing.Color.Snow;
            this.tcCategory.Controls.Add(this.panel26);
            this.tcCategory.Controls.Add(this.panel21);
            this.tcCategory.Controls.Add(this.dtgvViewCategory);
            this.tcCategory.Location = new System.Drawing.Point(4, 25);
            this.tcCategory.Name = "tcCategory";
            this.tcCategory.Padding = new System.Windows.Forms.Padding(3);
            this.tcCategory.Size = new System.Drawing.Size(988, 624);
            this.tcCategory.TabIndex = 1;
            this.tcCategory.Text = "Category";
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.SeaShell;
            this.panel26.Controls.Add(this.txtSearchCategory);
            this.panel26.Controls.Add(this.btnSearchCategory);
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Controls.Add(this.panel29);
            this.panel26.Location = new System.Drawing.Point(542, 9);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(437, 118);
            this.panel26.TabIndex = 9;
            // 
            // txtSearchCategory
            // 
            this.txtSearchCategory.Location = new System.Drawing.Point(39, 45);
            this.txtSearchCategory.Name = "txtSearchCategory";
            this.txtSearchCategory.Size = new System.Drawing.Size(218, 22);
            this.txtSearchCategory.TabIndex = 9;
            // 
            // btnSearchCategory
            // 
            this.btnSearchCategory.Location = new System.Drawing.Point(308, 31);
            this.btnSearchCategory.Name = "btnSearchCategory";
            this.btnSearchCategory.Size = new System.Drawing.Size(95, 50);
            this.btnSearchCategory.TabIndex = 8;
            this.btnSearchCategory.Text = "Search";
            this.btnSearchCategory.UseVisualStyleBackColor = true;
            this.btnSearchCategory.Click += new System.EventHandler(this.btnSearchCategory_Click);
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.panel28);
            this.panel27.Location = new System.Drawing.Point(556, 17);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(420, 118);
            this.panel27.TabIndex = 3;
            // 
            // panel28
            // 
            this.panel28.Location = new System.Drawing.Point(588, 14);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(398, 118);
            this.panel28.TabIndex = 2;
            // 
            // panel29
            // 
            this.panel29.Location = new System.Drawing.Point(588, 14);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(398, 118);
            this.panel29.TabIndex = 2;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.SeaShell;
            this.panel21.Controls.Add(this.panel24);
            this.panel21.Controls.Add(this.panel30);
            this.panel21.Controls.Add(this.panel25);
            this.panel21.Location = new System.Drawing.Point(542, 151);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(437, 393);
            this.panel21.TabIndex = 7;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.txtNameCategory);
            this.panel24.Controls.Add(this.label6);
            this.panel24.Location = new System.Drawing.Point(3, 76);
            this.panel24.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(431, 62);
            this.panel24.TabIndex = 3;
            // 
            // txtNameCategory
            // 
            this.txtNameCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameCategory.Location = new System.Drawing.Point(150, 17);
            this.txtNameCategory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNameCategory.Name = "txtNameCategory";
            this.txtNameCategory.Size = new System.Drawing.Size(278, 28);
            this.txtNameCategory.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "Category Name";
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.btnViewCategory);
            this.panel30.Controls.Add(this.btnDeleteCategory);
            this.panel30.Controls.Add(this.btnUpdateCategory);
            this.panel30.Controls.Add(this.btnAddCategory);
            this.panel30.Controls.Add(this.panel31);
            this.panel30.Controls.Add(this.panel33);
            this.panel30.Location = new System.Drawing.Point(3, 158);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(428, 245);
            this.panel30.TabIndex = 8;
            // 
            // btnViewCategory
            // 
            this.btnViewCategory.Location = new System.Drawing.Point(283, 165);
            this.btnViewCategory.Name = "btnViewCategory";
            this.btnViewCategory.Size = new System.Drawing.Size(95, 50);
            this.btnViewCategory.TabIndex = 7;
            this.btnViewCategory.Text = "xem";
            this.btnViewCategory.UseVisualStyleBackColor = true;
            this.btnViewCategory.Click += new System.EventHandler(this.btnViewCategory_Click);
            // 
            // btnDeleteCategory
            // 
            this.btnDeleteCategory.Location = new System.Drawing.Point(283, 31);
            this.btnDeleteCategory.Name = "btnDeleteCategory";
            this.btnDeleteCategory.Size = new System.Drawing.Size(95, 50);
            this.btnDeleteCategory.TabIndex = 6;
            this.btnDeleteCategory.Text = "xóa";
            this.btnDeleteCategory.UseVisualStyleBackColor = true;
            this.btnDeleteCategory.Click += new System.EventHandler(this.btnDeleteCategory_Click);
            // 
            // btnUpdateCategory
            // 
            this.btnUpdateCategory.Location = new System.Drawing.Point(52, 165);
            this.btnUpdateCategory.Name = "btnUpdateCategory";
            this.btnUpdateCategory.Size = new System.Drawing.Size(95, 50);
            this.btnUpdateCategory.TabIndex = 5;
            this.btnUpdateCategory.Text = "Sửa";
            this.btnUpdateCategory.UseVisualStyleBackColor = true;
            this.btnUpdateCategory.Click += new System.EventHandler(this.btnUpdateCategory_Click);
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.Location = new System.Drawing.Point(49, 31);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(95, 50);
            this.btnAddCategory.TabIndex = 4;
            this.btnAddCategory.Text = "Thêm";
            this.btnAddCategory.UseVisualStyleBackColor = true;
            this.btnAddCategory.Click += new System.EventHandler(this.btnAddCategory_Click);
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Location = new System.Drawing.Point(556, 17);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(420, 118);
            this.panel31.TabIndex = 3;
            // 
            // panel32
            // 
            this.panel32.Location = new System.Drawing.Point(588, 14);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(398, 118);
            this.panel32.TabIndex = 2;
            // 
            // panel33
            // 
            this.panel33.Location = new System.Drawing.Point(588, 14);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(398, 118);
            this.panel33.TabIndex = 2;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.txtIdCategory);
            this.panel25.Controls.Add(this.label7);
            this.panel25.Location = new System.Drawing.Point(3, 10);
            this.panel25.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(431, 62);
            this.panel25.TabIndex = 2;
            // 
            // txtIdCategory
            // 
            this.txtIdCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCategory.Location = new System.Drawing.Point(150, 16);
            this.txtIdCategory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtIdCategory.Name = "txtIdCategory";
            this.txtIdCategory.ReadOnly = true;
            this.txtIdCategory.Size = new System.Drawing.Size(278, 28);
            this.txtIdCategory.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(48, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 24);
            this.label7.TabIndex = 0;
            this.label7.Text = "ID";
            // 
            // dtgvViewCategory
            // 
            this.dtgvViewCategory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvViewCategory.BackgroundColor = System.Drawing.Color.SeaShell;
            this.dtgvViewCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvViewCategory.Location = new System.Drawing.Point(-4, 9);
            this.dtgvViewCategory.Name = "dtgvViewCategory";
            this.dtgvViewCategory.RowHeadersWidth = 51;
            this.dtgvViewCategory.RowTemplate.Height = 24;
            this.dtgvViewCategory.Size = new System.Drawing.Size(540, 647);
            this.dtgvViewCategory.TabIndex = 6;
            // 
            // tcFood
            // 
            this.tcFood.BackColor = System.Drawing.Color.Snow;
            this.tcFood.Controls.Add(this.dtgvFood);
            this.tcFood.Controls.Add(this.panel16);
            this.tcFood.Controls.Add(this.panel12);
            this.tcFood.Location = new System.Drawing.Point(4, 25);
            this.tcFood.Name = "tcFood";
            this.tcFood.Padding = new System.Windows.Forms.Padding(3);
            this.tcFood.Size = new System.Drawing.Size(988, 624);
            this.tcFood.TabIndex = 0;
            this.tcFood.Text = "Food";
            this.tcFood.Click += new System.EventHandler(this.tcFood_Click);
            // 
            // dtgvFood
            // 
            this.dtgvFood.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvFood.BackgroundColor = System.Drawing.Color.SeaShell;
            this.dtgvFood.ColumnHeadersHeight = 29;
            this.dtgvFood.Location = new System.Drawing.Point(6, 20);
            this.dtgvFood.Name = "dtgvFood";
            this.dtgvFood.ReadOnly = true;
            this.dtgvFood.RowHeadersWidth = 51;
            this.dtgvFood.RowTemplate.Height = 24;
            this.dtgvFood.Size = new System.Drawing.Size(496, 622);
            this.dtgvFood.TabIndex = 0;
            this.dtgvFood.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgvFood_CellContentClick);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.SeaShell;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.panel19);
            this.panel16.Controls.Add(this.panel8);
            this.panel16.Controls.Add(this.panel18);
            this.panel16.Controls.Add(this.panel17);
            this.panel16.Location = new System.Drawing.Point(532, 149);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(437, 493);
            this.panel16.TabIndex = 5;
            this.panel16.Paint += new System.Windows.Forms.PaintEventHandler(this.panel16_Paint);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.nmFoodPrice);
            this.panel20.Controls.Add(this.label3);
            this.panel20.Location = new System.Drawing.Point(3, 219);
            this.panel20.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(431, 62);
            this.panel20.TabIndex = 5;
            // 
            // nmFoodPrice
            // 
            this.nmFoodPrice.BackColor = System.Drawing.Color.Snow;
            this.nmFoodPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmFoodPrice.Location = new System.Drawing.Point(139, 17);
            this.nmFoodPrice.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nmFoodPrice.Name = "nmFoodPrice";
            this.nmFoodPrice.Size = new System.Drawing.Size(278, 28);
            this.nmFoodPrice.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Price";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.cbCategory);
            this.panel19.Controls.Add(this.lbCategory);
            this.panel19.Location = new System.Drawing.Point(3, 142);
            this.panel19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(431, 62);
            this.panel19.TabIndex = 4;
            // 
            // cbCategory
            // 
            this.cbCategory.BackColor = System.Drawing.Color.Snow;
            this.cbCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(139, 18);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(278, 26);
            this.cbCategory.TabIndex = 1;
            // 
            // lbCategory
            // 
            this.lbCategory.AutoSize = true;
            this.lbCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCategory.Location = new System.Drawing.Point(13, 20);
            this.lbCategory.Name = "lbCategory";
            this.lbCategory.Size = new System.Drawing.Size(85, 24);
            this.lbCategory.TabIndex = 0;
            this.lbCategory.Text = "Category";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnViewFood);
            this.panel8.Controls.Add(this.btnDeleteFood);
            this.panel8.Controls.Add(this.btnUpdateFood);
            this.panel8.Controls.Add(this.btnAddFood);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(20, 300);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(400, 165);
            this.panel8.TabIndex = 1;
            // 
            // btnViewFood
            // 
            this.btnViewFood.Location = new System.Drawing.Point(263, 99);
            this.btnViewFood.Name = "btnViewFood";
            this.btnViewFood.Size = new System.Drawing.Size(95, 50);
            this.btnViewFood.TabIndex = 7;
            this.btnViewFood.Text = "xem";
            this.btnViewFood.UseVisualStyleBackColor = true;
            this.btnViewFood.Click += new System.EventHandler(this.btnViewFood_Click);
            // 
            // btnDeleteFood
            // 
            this.btnDeleteFood.Location = new System.Drawing.Point(263, 3);
            this.btnDeleteFood.Name = "btnDeleteFood";
            this.btnDeleteFood.Size = new System.Drawing.Size(95, 50);
            this.btnDeleteFood.TabIndex = 6;
            this.btnDeleteFood.Text = "xóa";
            this.btnDeleteFood.UseVisualStyleBackColor = true;
            this.btnDeleteFood.Click += new System.EventHandler(this.btnDeleteFood_Click);
            // 
            // btnUpdateFood
            // 
            this.btnUpdateFood.Location = new System.Drawing.Point(48, 99);
            this.btnUpdateFood.Name = "btnUpdateFood";
            this.btnUpdateFood.Size = new System.Drawing.Size(95, 50);
            this.btnUpdateFood.TabIndex = 5;
            this.btnUpdateFood.Text = "Sửa";
            this.btnUpdateFood.UseVisualStyleBackColor = true;
            this.btnUpdateFood.Click += new System.EventHandler(this.btnUpdateFood_Click);
            // 
            // btnAddFood
            // 
            this.btnAddFood.Location = new System.Drawing.Point(48, 3);
            this.btnAddFood.Name = "btnAddFood";
            this.btnAddFood.Size = new System.Drawing.Size(95, 50);
            this.btnAddFood.TabIndex = 4;
            this.btnAddFood.Text = "Thêm";
            this.btnAddFood.UseVisualStyleBackColor = true;
            this.btnAddFood.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Location = new System.Drawing.Point(556, 17);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(420, 118);
            this.panel10.TabIndex = 3;
            // 
            // panel11
            // 
            this.panel11.Location = new System.Drawing.Point(588, 14);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(398, 118);
            this.panel11.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.Location = new System.Drawing.Point(588, 14);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(398, 118);
            this.panel9.TabIndex = 2;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.txtFoodName);
            this.panel18.Controls.Add(this.label2);
            this.panel18.Location = new System.Drawing.Point(3, 76);
            this.panel18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(431, 62);
            this.panel18.TabIndex = 3;
            this.panel18.Paint += new System.Windows.Forms.PaintEventHandler(this.panel18_Paint);
            // 
            // txtFoodName
            // 
            this.txtFoodName.BackColor = System.Drawing.Color.Snow;
            this.txtFoodName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFoodName.Location = new System.Drawing.Point(139, 17);
            this.txtFoodName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFoodName.Name = "txtFoodName";
            this.txtFoodName.Size = new System.Drawing.Size(278, 28);
            this.txtFoodName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Drink Name";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.SeaShell;
            this.panel17.Controls.Add(this.txtFoodID);
            this.panel17.Controls.Add(this.label1);
            this.panel17.Location = new System.Drawing.Point(3, 10);
            this.panel17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(431, 62);
            this.panel17.TabIndex = 2;
            // 
            // txtFoodID
            // 
            this.txtFoodID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFoodID.Location = new System.Drawing.Point(139, 17);
            this.txtFoodID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFoodID.Name = "txtFoodID";
            this.txtFoodID.ReadOnly = true;
            this.txtFoodID.Size = new System.Drawing.Size(278, 28);
            this.txtFoodID.TabIndex = 1;
            this.txtFoodID.TextChanged += new System.EventHandler(this.txtFoodID_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.SeaShell;
            this.panel12.Controls.Add(this.txtFindFood);
            this.panel12.Controls.Add(this.btnFindFood);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Controls.Add(this.panel15);
            this.panel12.Location = new System.Drawing.Point(535, 20);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(434, 118);
            this.panel12.TabIndex = 4;
            // 
            // txtFindFood
            // 
            this.txtFindFood.BackColor = System.Drawing.Color.Snow;
            this.txtFindFood.Location = new System.Drawing.Point(39, 45);
            this.txtFindFood.Name = "txtFindFood";
            this.txtFindFood.Size = new System.Drawing.Size(218, 22);
            this.txtFindFood.TabIndex = 9;
            // 
            // btnFindFood
            // 
            this.btnFindFood.Location = new System.Drawing.Point(308, 31);
            this.btnFindFood.Name = "btnFindFood";
            this.btnFindFood.Size = new System.Drawing.Size(95, 50);
            this.btnFindFood.TabIndex = 8;
            this.btnFindFood.Text = "xem";
            this.btnFindFood.UseVisualStyleBackColor = true;
            this.btnFindFood.Click += new System.EventHandler(this.btnFindFood_Click);
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Location = new System.Drawing.Point(556, 17);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(420, 118);
            this.panel13.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.Location = new System.Drawing.Point(588, 14);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(398, 118);
            this.panel14.TabIndex = 2;
            // 
            // panel15
            // 
            this.panel15.Location = new System.Drawing.Point(588, 14);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(398, 118);
            this.panel15.TabIndex = 2;
            // 
            // tcAdmin
            // 
            this.tcAdmin.Controls.Add(this.tcFood);
            this.tcAdmin.Controls.Add(this.tcCategory);
            this.tcAdmin.Controls.Add(this.tcTable);
            this.tcAdmin.Controls.Add(this.tcAccount);
            this.tcAdmin.Location = new System.Drawing.Point(12, 50);
            this.tcAdmin.Name = "tcAdmin";
            this.tcAdmin.SelectedIndex = 0;
            this.tcAdmin.Size = new System.Drawing.Size(996, 653);
            this.tcAdmin.TabIndex = 0;
            // 
            // btnLongExit
            // 
            this.btnLongExit.BackColor = System.Drawing.Color.Transparent;
            this.btnLongExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLongExit.BackgroundImage")));
            this.btnLongExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLongExit.FlatAppearance.BorderSize = 0;
            this.btnLongExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLongExit.Location = new System.Drawing.Point(920, 12);
            this.btnLongExit.Name = "btnLongExit";
            this.btnLongExit.Size = new System.Drawing.Size(81, 52);
            this.btnLongExit.TabIndex = 1;
            this.btnLongExit.UseVisualStyleBackColor = false;
            this.btnLongExit.Click += new System.EventHandler(this.btnLongExit_Click);
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1020, 715);
            this.Controls.Add(this.btnLongExit);
            this.Controls.Add(this.tcAdmin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAdmin";
            this.tcAccount.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmTypeAccount)).EndInit();
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvViewAccount)).EndInit();
            this.tcTable.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvViewTable)).EndInit();
            this.tcCategory.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvViewCategory)).EndInit();
            this.tcFood.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvFood)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFoodPrice)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.tcAdmin.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tcAccount;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtSearchAccount;
        private System.Windows.Forms.Button btnFindAccount;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Button btnDeleteAccount;
        private System.Windows.Forms.Button btnUpdateAccount;
        private System.Windows.Forms.Button btnAddAccount;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.TextBox txtDisplayNameAccount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.TextBox txtUserNameAccount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dtgvViewAccount;
        private System.Windows.Forms.TabPage tcTable;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Button btnViewTable;
        private System.Windows.Forms.Button btnDeleteTable;
        private System.Windows.Forms.Button btnUpdateTable;
        private System.Windows.Forms.Button btnAddTable;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.ComboBox cbStatusTable;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.TextBox txtNameTable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.TextBox txtIDTable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dtgvViewTable;
        private System.Windows.Forms.TabPage tcCategory;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.TextBox txtSearchCategory;
        private System.Windows.Forms.Button btnSearchCategory;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.TextBox txtNameCategory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Button btnViewCategory;
        private System.Windows.Forms.Button btnDeleteCategory;
        private System.Windows.Forms.Button btnUpdateCategory;
        private System.Windows.Forms.Button btnAddCategory;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dtgvViewCategory;
        private System.Windows.Forms.TabPage tcFood;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.NumericUpDown nmFoodPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label lbCategory;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnViewFood;
        private System.Windows.Forms.Button btnDeleteFood;
        private System.Windows.Forms.Button btnUpdateFood;
        private System.Windows.Forms.Button btnAddFood;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox txtFoodName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox txtFoodID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox txtFindFood;
        private System.Windows.Forms.Button btnFindFood;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.TabControl tcAdmin;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.DataGridView dtgvFood;
        private System.Windows.Forms.NumericUpDown nmTypeAccount;
        private System.Windows.Forms.Button btnLoadAccount;
        private System.Windows.Forms.TextBox txtIdCategory;
        private System.Windows.Forms.Button btnLongExit;
    }
}