﻿using CoffeeShop.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class frmBillTotal : Form
    {
        public frmBillTotal()
        {
            InitializeComponent();
            LoadListBillByDate(dtDataTime.Value);
        }
        void LoadListBillByDate(DateTime checkOut)
        {
            dtgvBill.DataSource = BillDAO.Instance.GetBillListByDate(checkOut);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            LoadListBillByDate(dtDataTime.Value);
        }

        private void btnLongExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
