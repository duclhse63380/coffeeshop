﻿using CoffeeShop.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class frmRegisterMember : Form
    {
        BindingSource listMember = new BindingSource();
        public frmRegisterMember()
        {
            InitializeComponent();
        }
        void AddMember(string name, string phone, string gender)
        {
            if (MemberDAO.Instance.InsertMember(name, phone, gender))
            {
                MessageBox.Show("Add Member Successfull ");
            }
            else
            {
                MessageBox.Show("Add Member Fail");
            }

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (CheckInput())
            {
                string name = txtName.Text;
                string phone = txtPhone.Text;
                string gender = txtGender.Text;
                    AddMember(name, phone, gender);
                    txtName.Text = "";
                    txtPhone.Text = "";
                    txtGender.Text = "";
                
               
            }
            else
            {
                MessageBox.Show("Please fill all text box!");
            }
        }
     
        private bool CheckInput()
        {
            string Name = txtPhone.Text.Trim();
            string Gender = txtGender.Text.Trim();
            string Display = txtName.Text.Trim();
            if (Name.Length == 0 || Display.Length == 0 || Gender.Length == 0)
            {
                return false;
            }
            return true;
        }
        private void btnLongExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmRegisterMember_Load(object sender, EventArgs e)
        {

        }
    }
}
