﻿namespace CoffeeShop
{
    partial class frmBillTotal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillTotal));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtgvBill = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnView = new System.Windows.Forms.Button();
            this.dtDataTime = new System.Windows.Forms.DateTimePicker();
            this.btnLongExit = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvBill)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtgvBill);
            this.panel1.Location = new System.Drawing.Point(23, 151);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(845, 532);
            this.panel1.TabIndex = 0;
            // 
            // dtgvBill
            // 
            this.dtgvBill.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvBill.BackgroundColor = System.Drawing.Color.SeaShell;
            this.dtgvBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvBill.Location = new System.Drawing.Point(0, 0);
            this.dtgvBill.Name = "dtgvBill";
            this.dtgvBill.ReadOnly = true;
            this.dtgvBill.RowHeadersWidth = 51;
            this.dtgvBill.RowTemplate.Height = 24;
            this.dtgvBill.Size = new System.Drawing.Size(845, 532);
            this.dtgvBill.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.Controls.Add(this.btnView);
            this.panel2.Controls.Add(this.dtDataTime);
            this.panel2.Location = new System.Drawing.Point(208, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(498, 79);
            this.panel2.TabIndex = 1;
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.MistyRose;
            this.btnView.Location = new System.Drawing.Point(397, 26);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(85, 33);
            this.btnView.TabIndex = 1;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // dtDataTime
            // 
            this.dtDataTime.CalendarForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtDataTime.CalendarMonthBackground = System.Drawing.Color.MistyRose;
            this.dtDataTime.Location = new System.Drawing.Point(55, 29);
            this.dtDataTime.Name = "dtDataTime";
            this.dtDataTime.Size = new System.Drawing.Size(200, 22);
            this.dtDataTime.TabIndex = 0;
            this.dtDataTime.TabStop = false;
            this.dtDataTime.Value = new System.DateTime(2020, 4, 3, 0, 0, 0, 0);
            // 
            // btnLongExit
            // 
            this.btnLongExit.BackColor = System.Drawing.Color.Transparent;
            this.btnLongExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLongExit.BackgroundImage")));
            this.btnLongExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLongExit.FlatAppearance.BorderSize = 0;
            this.btnLongExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLongExit.Location = new System.Drawing.Point(775, 12);
            this.btnLongExit.Name = "btnLongExit";
            this.btnLongExit.Size = new System.Drawing.Size(81, 52);
            this.btnLongExit.TabIndex = 2;
            this.btnLongExit.UseVisualStyleBackColor = false;
            this.btnLongExit.Click += new System.EventHandler(this.btnLongExit_Click);
            // 
            // frmBillTotal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(894, 695);
            this.Controls.Add(this.btnLongExit);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBillTotal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BillTotal";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvBill)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dtgvBill;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.DateTimePicker dtDataTime;
        private System.Windows.Forms.Button btnLongExit;
    }
}