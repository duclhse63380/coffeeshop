﻿using CoffeeShop.DAO;
using CoffeeShop.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class frmAdmin : Form
    {
        BindingSource listFood = new BindingSource();
        BindingSource listAccount = new BindingSource();
        BindingSource listCategory = new BindingSource();
        BindingSource listTable = new BindingSource();
        public frmAdmin()
        {
            InitializeComponent();
            Loadda();
        }
        void Loadda()
        {
            dtgvViewTable.DataSource = listTable;
            dtgvFood.DataSource = listFood;
            dtgvViewAccount.DataSource = listAccount;
            dtgvViewCategory.DataSource = listCategory;
            LoadListFood();
            AddFoodAD();
            LoadAccount();
            AddAccountBinding();
            AddCategoryBinding();
            LoadCategory();
            LoadCategoryIntoComboBox(cbCategory);
            LoadListTable();
            AddTableBinding();

        }
        // -----------Account--------------------
        // -----------Account--------------------
        // -----------Account--------------------
        void LoadAccount()
        {
            listAccount.DataSource = AccountDAO.Instance.GetListAccount();
        }
        void AddAccount(string name, string displayname, int type)
        {
            if (AccountDAO.Instance.InsertAccount(name, displayname, type))
            {
                MessageBox.Show("Add Account Successfull and  Password is 0");
            }
            else
            {
                MessageBox.Show("Add Account Fail");
            }
            LoadAccount();
        }
        void UpdateAccount(string name, string displayname, int type)
        {
            if (AccountDAO.Instance.UpdateAccount(name, displayname, type))
            {
                MessageBox.Show("Udapte Account Successfull");
            }
            else
            {
                MessageBox.Show("Update Account Fail");
            }
            LoadAccount();
        }
        void DeleleAccount(string name)
        {
            if (AccountDAO.Instance.DeleteAccount(name))
            {   
                MessageBox.Show("Delete Account Successfull");
            }
            else
            {
                MessageBox.Show("Delete Account Fail");
            }
            LoadAccount();
        }
        private void btnViewAccount_Click(object sender, EventArgs e)
        {
            LoadAccount();
        }

        private void btnFindAccount_Click(object sender, EventArgs e)
        {
            string name = txtSearchAccount.Text;
            listAccount.DataSource = AccountDAO.Instance.SearchAccoutByDisplayName(name);

        }

        private void btnLoadAccount_Click(object sender, EventArgs e)
        {
            LoadAccount();
        }

        private void btnAddAccount_Click_1(object sender, EventArgs e)
        {
            if (CheckInput())
            {
                string userName = txtUserNameAccount.Text;
                string displayName = txtDisplayNameAccount.Text;
                int typeAccount = (int)nmTypeAccount.Value;
                AddAccount(userName, displayName, typeAccount);
            }
            else
            {
                MessageBox.Show("Please fill all text box!");
            }
        }
        private void btnUpdateAccount_Click(object sender, EventArgs e)
        {
            string userName = txtUserNameAccount.Text;
            string displayName = txtDisplayNameAccount.Text;
            int typeAccount = (int)nmTypeAccount.Value;
            UpdateAccount(userName, displayName, typeAccount);
        }
        private void btnDeleteAccount_Click(object sender, EventArgs e)
        {
            string userName = txtUserNameAccount.Text;
            DeleleAccount(userName);
        }
        void AddAccountBinding()
        {
            txtUserNameAccount.DataBindings.Add(new Binding("Text",dtgvViewAccount.DataSource,"UserName",true, DataSourceUpdateMode.Never));
            txtDisplayNameAccount.DataBindings.Add(new Binding("Text", dtgvViewAccount.DataSource, "DisplayName", true, DataSourceUpdateMode.Never));
            nmTypeAccount.DataBindings.Add(new Binding("Value", dtgvViewAccount.DataSource, "Type", true, DataSourceUpdateMode.Never));
        }
        private bool CheckInput()
        {
            string Name = txtUserNameAccount.Text.Trim();
            string Display = txtDisplayNameAccount.Text.Trim();
            if (Name.Length == 0 || Display.Length == 0)
            {
                return false;
            }
            return true;
        }
        //-----------------Category-----------------
        //-----------------Category-----------------
        //-----------------Category-----------------
        void AddCategoryBinding()
        {
            txtNameCategory.DataBindings.Add(new Binding("Text", dtgvViewCategory.DataSource,"name", true, DataSourceUpdateMode.Never));
           txtIdCategory.DataBindings.Add(new Binding("Text", dtgvViewCategory.DataSource, "id", true, DataSourceUpdateMode.Never));
        }
        void LoadCategory()
        {
            listCategory.DataSource = CategoryDAO.Instance.GetListCategory();
        }
        void LoadCategoryIntoComboBox(ComboBox comboBox)
        {
            cbCategory.DataSource = CategoryDAO.Instance.GetListCategory();
            cbCategory.DisplayMember = "Name";
        }
        void AddCategory( string name)
        {
            if (CategoryDAO.Instance.InsertCategory(name))
            {
                MessageBox.Show("Add Category Successfull ");
            }
            else
            {
                MessageBox.Show("Add Category Fail");
            }
            LoadCategory();
        }
        void UpdateCategory(string name , int id)
        {
            if (CategoryDAO.Instance.UpdateCategory(name , id))
            {
                MessageBox.Show("Udapte Category Successfull");
            }
            else
            {
                MessageBox.Show("Update Category Fail");
            }
            LoadCategory();
        }
        void DeleleCategory( int id)
        {
            if (CategoryDAO.Instance.DeleteCategory(id))
            {
                MessageBox.Show("Delete Category Successfull");
            }
            else
            {
                MessageBox.Show("Delete Category Fail");
            }
            LoadCategory();
        }
        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            string name = txtNameCategory.Text;
            AddCategory(name);
        }

        private void btnDeleteCategory_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtIdCategory.Text);
            DeleleCategory(id);

        }

        private void btnUpdateCategory_Click(object sender, EventArgs e)
        {
            string name = txtNameCategory.Text;
            int id = Convert.ToInt32(txtIdCategory.Text);
            UpdateCategory(name , id);

        }

        private void btnViewCategory_Click(object sender, EventArgs e)
        {
            LoadCategory();
        }
        private void btnSearchCategory_Click(object sender, EventArgs e)
        {
            string name = txtSearchCategory.Text;
            listCategory.DataSource = CategoryDAO.Instance.SearchCategoryName(name);

        }
        //-------------------- Food--------------------------
        //-------------------- Food--------------------------
        //-------------------- Food--------------------------
        private event EventHandler insertFood;
        public event EventHandler InsertFood
        {
            add { insertFood += value; }
            remove { insertFood -= value; }
        }

        private event EventHandler deleteFood;
        public event EventHandler DeleteFood
        {
            add { deleteFood += value; }
            remove { deleteFood -= value; }
        }

        private event EventHandler updateFood;
        public event EventHandler UpdateFood
        {
            add { updateFood += value; }
            remove { updateFood -= value; }
        }
        void AddFoodAD()
        {
            txtFoodName.DataBindings.Add(new Binding("Text", dtgvFood.DataSource, "name", true, DataSourceUpdateMode.Never));
            txtFoodID.DataBindings.Add(new Binding("Text", dtgvFood.DataSource, "id", true, DataSourceUpdateMode.Never));
            nmFoodPrice.DataBindings.Add(new Binding("Text", dtgvFood.DataSource, "price", true, DataSourceUpdateMode.Never));
           
         
        }      
        private void tcFood_Click(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            string name = txtFoodName.Text;
            int idCategory = (cbCategory.SelectedItem as Category).ID;
            float price = (float)nmFoodPrice.Value;
            if (FoodDAO.Instance.InsertFood(name,idCategory, price))
            {
                MessageBox.Show("Add Food Successfull !");
                LoadListFood();
                if (insertFood != null)
                    insertFood(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Add Food Fail !");
            }
        }
        void LoadListFood()
        {
           listFood.DataSource = FoodDAO.Instance.GetListFoods();
        }
        private void btnViewFood_Click(object sender, EventArgs e)
        {
            LoadListFood();
        }
        private void txtFoodID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dtgvFood.SelectedCells.Count > 0)
                {


                    int id = (int)dtgvFood.SelectedCells[0].OwningRow.Cells["CategoryID"].Value;

                    Category category = CategoryDAO.Instance.GetCategoryById(id);
                    cbCategory.SelectedItem = category;
                    int index = -1;
                    int i = 0;
                    foreach (Category item in cbCategory.Items)
                    {
                        if (item.ID == category.ID)
                        {
                            index = i;
                            break;
                        }
                        i++;
                    }
                    cbCategory.SelectedIndex = index;
                }
            }
            catch
            { }
            
         
           
        }

        private void btnUpdateFood_Click(object sender, EventArgs e)
        {
            string name = txtFoodName.Text;
            int idCategory = (cbCategory.SelectedItem as Category).ID;
            float price = (float)nmFoodPrice.Value;
            int id = Convert.ToInt32(txtFoodID.Text);
            if (FoodDAO.Instance.UpdateFood(id , name, idCategory, price))
            {
                MessageBox.Show("Update Food Successfull !");
                LoadListFood();
                if (updateFood != null)
                    updateFood(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Update Food Fail !");
            }
        }

        private void btnDeleteFood_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtFoodID.Text);
            if (FoodDAO.Instance.DeleteFood(id))
            {
                MessageBox.Show("Delete Food Successfull !");
                LoadListFood();
                if (deleteFood != null)
                    deleteFood(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Delete Food Fail !");
            }
        }
        List<Food> SearchFoodByName(string name)
        {
            List<Food> Foodlist = new List<Food>();
            Foodlist = FoodDAO.Instance.SearchFoodByName(name);
            return Foodlist;
        }

        private void btnFindFood_Click(object sender, EventArgs e)
        {
           listFood.DataSource = SearchFoodByName(txtFindFood.Text);
        }
        //-------------------- Table--------------------------
        //-------------------- Table--------------------------
        //-------------------- Table--------------------------
        void AddTable(string name ,string status)
        {
            if (TableDAO.Instance.InsertTable(name , status))
            {
                MessageBox.Show("Add Table Successfull ");
            }
            else
            {
                MessageBox.Show("Add Table Fail");
            }
            LoadCategory();
        }
        void UpdateTable(int id, string name, string status)
        {
            if (TableDAO.Instance.UpdateTable(id, name , status))
            {
                MessageBox.Show("Udapte Table Successfull");
            }
            else
            {
                MessageBox.Show("Update Table Fail");
            }
            LoadCategory();
        }
        void DeleleTable(int id)
        {
            if (TableDAO.Instance.DeleteTable(id))
            {
                MessageBox.Show("Delete TableSuccessfull");
            }
            else
            {
                MessageBox.Show("Delete Table Fail");
            }
            LoadCategory();
        }
        private void btnAddTable_Click(object sender, EventArgs e)
        {
            string name = txtNameTable.Text;
            string status = cbStatusTable.Text;
            
            AddTable(name,status);
            LoadListTable();
        }

        private void btnDeleteTable_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtIDTable.Text);
            DeleleTable(id);
            LoadListTable();
        }

        private void btnUpdateTable_Click(object sender, EventArgs e)
        {
            string name = txtNameTable.Text;
            string status = cbStatusTable.Text;
            int id = Convert.ToInt32(txtIDTable.Text);
            UpdateTable(id, name, status);
            LoadListTable();
        }

        private void btnViewTable_Click(object sender, EventArgs e)
        {
            LoadListTable();
        }
        void LoadListTable()
        {
            listTable.DataSource = TableDAO.Instance.GetListTable();
        }
        void AddTableBinding()
        {
            txtIDTable.DataBindings.Add(new Binding("Text", dtgvViewTable.DataSource, "id", true, DataSourceUpdateMode.Never));
            txtNameTable.DataBindings.Add(new Binding("Text", dtgvViewTable.DataSource, "name", true, DataSourceUpdateMode.Never));
            cbStatusTable.DataBindings.Add(new Binding("Text", dtgvViewTable.DataSource, "status", true, DataSourceUpdateMode.Never));


        }












































        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel18_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel16_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel43_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dtgvFood_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtIDCategory_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnLongExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
