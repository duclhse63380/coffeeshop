﻿using CoffeeShop.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class MemberProfile : Form
    {
        BindingSource listMember = new BindingSource();
        public MemberProfile()
        {
            InitializeComponent();
            Loadlist();
        }

        void Loadlist()
        {
            dtgvMember.DataSource = listMember;
            LoadMember();
            MemberBinding();
        }
        void LoadMember()
        {
            listMember.DataSource = MemberDAO.Instance.GetListMember();
        }
        void MemberBinding()
        {
            txtNameMember.DataBindings.Add(new Binding("Text", dtgvMember.DataSource, "name", true, DataSourceUpdateMode.Never));
            txtPhoneMember.DataBindings.Add(new Binding("Text", dtgvMember.DataSource, "phone", true, DataSourceUpdateMode.Never));
            txtGenderMember.DataBindings.Add(new Binding("Text", dtgvMember.DataSource, "gender", true, DataSourceUpdateMode.Never));
           txtPointMember.DataBindings.Add(new Binding("Text", dtgvMember.DataSource, "totalPoint", true, DataSourceUpdateMode.Never));
        }

        private void btnViewMember_Click(object sender, EventArgs e)
        {
            LoadMember();
        }
        void DeleleMember(string phone)
        {
            if (MemberDAO.Instance.DeleteMember(phone))
            {
                MessageBox.Show("Delete Member Successfull");
            }
            else
            {
                MessageBox.Show("Delete Member Fail");
            }
           LoadMember();
        }

        private void btnDeleteMember_Click(object sender, EventArgs e)
        {
            string phone = txtPhoneMember.Text;
            DeleleMember(phone);
        }

        private void btnSearchMember_Click(object sender, EventArgs e)
        {
            string phone = txtSearchMember.Text;
            listMember.DataSource = MemberDAO.Instance.SearchMemberByPhone(phone);
           
            
        }

        private void btnLongExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
