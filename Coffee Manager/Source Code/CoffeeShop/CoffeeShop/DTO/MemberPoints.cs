﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop.DTO
{
    public class MemberPoints
    {
        private string phone;
        private float point;

        public MemberPoints( string phone, float point)
        {
            
            this.phone = phone;
            this.point = point;
        }
        public MemberPoints(DataRow row)
        {  
            this.Phone = row["phone"].ToString();
            this.Point = (float)Convert.ToDouble(row["point"].ToString());
        }
       
        public string Phone { get => phone; set => phone = value; }
        public float Point { get => point; set => point = value; }
    }
}
