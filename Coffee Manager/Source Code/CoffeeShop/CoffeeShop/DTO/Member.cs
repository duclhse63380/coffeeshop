﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop.DTO
{
    public class Member
    {
        
        private string name;
        private string phone;
        private string gender;
        private float totalPoint;

        public Member(string name, string phone, string gender, float totalPoint)
        {
            
            this.Name = name;
            this.Phone = phone;
            this.Gender = gender;
            this.TotalPoint = totalPoint;
        }
        public Member(DataRow row)
        {
            
           
            this.Name = row["name"].ToString();
            this.Phone = row["phone"].ToString();
            this.Gender = row["gender"].ToString();
            this.totalPoint = (float)Convert.ToDouble(row["totalPoint"].ToString());
        }

        public string Name { get => name; set => name = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Gender { get => gender; set => gender = value; }
        public float TotalPoint{ get => totalPoint; set => totalPoint = value; }
    }
}
