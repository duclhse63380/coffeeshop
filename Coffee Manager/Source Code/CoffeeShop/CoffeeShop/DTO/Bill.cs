﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop.DTO
{
   public class Bill
    {
        private int iD;
        private DateTime? dateCheckIn;
        private DateTime? dateCheckOut;
        private int discount;
        private int status;
       private string displayNameAccount;
        public Bill(string displayNameAccount, int iD, DateTime? dateCheckIn, DateTime? dateCheckOut,int discount, int status)
        {
            this.DisplayNameAccount = displayNameAccount;
            this.ID = iD;
            this.DateCheckIn = dateCheckIn;
            this.DateCheckOut = dateCheckOut;
            this.Discount = discount;
            this.Status = status;
           
        }
        public Bill(DataRow row)
        {
            this.ID = (int)row["id"];
            this.DisplayNameAccount = row["displayName"].ToString();
            this.DateCheckIn = (DateTime?)row["dateCheckIn"];
            var dateCheckOutTemp = row["dateCheckOut"];
            if (dateCheckOutTemp.ToString() != "")
                this.DateCheckOut = (DateTime?)dateCheckOutTemp;
            if (row["discount"].ToString() != "")
            {
                this.Discount = (int)row["discount"];
            }
            this.Status = (int)row["status"];
            
        }

        public int ID { get => iD; set => iD = value; }
        public DateTime? DateCheckIn { get => dateCheckIn; set => dateCheckIn = value; }
        public DateTime? DateCheckOut { get => dateCheckOut; set => dateCheckOut = value; }
        public int Status { get => status; set => status = value; }
        public int Discount { get => discount; set => discount = value; }
        public string DisplayNameAccount { get => displayNameAccount; set => displayNameAccount = value; }
    }
}
