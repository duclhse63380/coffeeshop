﻿using CoffeeShop.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop.DAO
{
    public class MemberDAO
    {
        private static MemberDAO instance;
        public static MemberDAO Instance
        {
            get
            {
                if (instance == null) instance = new MemberDAO();
                return MemberDAO.instance;
            }
            private set { MemberDAO.instance = value; }
        }
        private MemberDAO() { }
        public bool InsertMember(string name, string phone, string gender)
        {
            string query = string.Format("Insert Member ( name, phone, gender )Values (N'{0}', N'{1}', N'{2}') ", name, phone, gender);
            int result = DataProvider.Instance.ExecuteNonQuery(query);
            return result > 0;
        }
        public List<Member> GetListMember(string phone)
        {
            List<Member> list = new List<Member>();
            string query = " SELECT * FROM dbo.Member WHERE phone = " + phone;
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Member member = new Member(item);
                list.Add(member);
            }
            return list;
        }
        public DataTable GetListMember()
        {
            return DataProvider.Instance.ExecuteQuery("Select name , phone  , gender , totalPoint  from dbo.Member ");
        }
        public bool UpdateMember(float totalPoint, string phone)
        {
            string query = string.Format("Update Member Set totalPoint ={0} where phone = N'{1}'", totalPoint, phone);
            int result = DataProvider.Instance.ExecuteNonQuery(query);
            return result > 0;
        }
        public bool DeleteMember(string phone)
        {
            string query = string.Format("Delete Member where phone = N'{0}' ", phone);
            int result = DataProvider.Instance.ExecuteNonQuery(query);
            return result > 0;
        }
        public DataTable SearchMemberByPhone(string phone)
        {
            string query = string.Format("select * from Member where phone like N'%{0}%'",phone);
            return DataProvider.Instance.ExecuteQuery(query);

        }
        public DataTable SearchCheckDuplicate(string phone)
        {
            string query = string.Format("SELECT phone FROM Member where phone = N'{0}'", phone);
            return DataProvider.Instance.ExecuteQuery(query);
           
        }
    }
}
