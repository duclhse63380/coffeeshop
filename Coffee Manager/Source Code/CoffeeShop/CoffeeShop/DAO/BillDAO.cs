﻿using CoffeeShop.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop.DAO
{
    public class BillDAO
    {
        private static BillDAO instance;
        public static BillDAO Instance
        {
            get
            {
                if (instance == null) instance = new BillDAO();
                return BillDAO.instance;
            }
            private set { BillDAO.instance = value; }
        }
        private BillDAO() { }
        // thanh cong bill ID
        // that bai bill -1
        public int GetUncheckBillIdbyTableId(int id)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("Select * from dbo.Bill where idTable ="+ id +"and status = 0");
            if (data.Rows.Count >0)
            {
                Bill bill = new Bill(data.Rows[0]);
                return bill.ID; // lay id cua bill
            }
            return -1;


        }
        public void InsertBill(int idTable , string displayName)
        {
            DataProvider.Instance.ExecuteNonQuery("exec USP_InsertBill @idTable , @displayName  ", new object[] {idTable,displayName});
            
        }
        public int getMaxIdBill()
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar("Select MAX(id) from dbo.Bill");
            }
            catch
            {

                return 1;
            }
            
        }
        public void checkOut(int id, int discount , float totalPrice)
        {
            string query = "Update dbo.Bill set dateCheckOut = GETDATE(), status = 1, "+" discount = "+ discount + ", totalPrice = " + totalPrice + " where id = " + id;
            DataProvider.Instance.ExecuteNonQuery(query);
        }
        public DataTable GetBillListByDate( DateTime checkOut)
        {
           return DataProvider.Instance.ExecuteQuery("exec USP_GetListBillByDate  @checkOut  ", new object[] {checkOut});

        }
    }
}
