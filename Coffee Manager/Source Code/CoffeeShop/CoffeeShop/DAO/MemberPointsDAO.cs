﻿using CoffeeShop.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop.DAO
{
    public class MemberPointsDAO
    {
        private static MemberPointsDAO instance;
        public static MemberPointsDAO Instance
        {
            get
            {
                if (instance == null) instance = new MemberPointsDAO();
                return MemberPointsDAO.instance;
            }
            private set { MemberPointsDAO.instance = value; }
        }
        private MemberPointsDAO() { }

        public bool InsertMemberPoints(string phone , float point)
        {
            string query = string.Format("Insert MemberPoints ( phone , point ) Values (N'{0}', {1})", phone , point);
            int result = DataProvider.Instance.ExecuteNonQuery(query);
            return result > 0;
        }
        public List<MemberPoints> GetListMemberPoints(string phone)
        {
            List<MemberPoints> list = new List<MemberPoints>();
            string query ="SELECT p.phone, p.point FROM dbo.Member AS m, dbo.MemberPoints AS p WHERE p.phone = m.phone and m.phone = " + phone ;
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            foreach (DataRow item in data.Rows)
            {
                MemberPoints member = new MemberPoints(item);
                list.Add(member);
            }
            return list;
        }
    }
}
