﻿using CoffeeShop.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop.DAO
{
    
    class AccountDAO
    {
        private static AccountDAO instance;
        public  static AccountDAO Instance
        {
            get { if (instance == null) instance = new AccountDAO();
                return instance;
            }
            private set { instance = value; }
        }
        private AccountDAO() { }
        public bool Login(string userName, string passWord)
        {
            string SQL = "USP_Login @userName , @password";
            DataTable result = DataProvider.Instance.ExecuteQuery(SQL, new object[] {userName, passWord});
            return result.Rows.Count > 0;
        }
        public Account GetAccount(string userName)
        {
            DataTable result = DataProvider.Instance.ExecuteQuery("Select * from account where userName = '" + userName + "'");
            foreach (DataRow item in result.Rows)
            {
                return new Account(item);
            }
            return null;
        }
        public bool UpdateAccount(string userName, string displayName,string pass, string newPass)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("exec US_UpdateAccount @userName  , @displayName , @password , @newPassword ",new object[]{userName,displayName,pass,newPass});
            return result > 0; // nếu như số dòng được thay đổi lớn hown0 có ngĩa update thành công
        }

        public DataTable GetListAccount()
        {
            return DataProvider.Instance.ExecuteQuery("Select UserName, DisplayName, Type from Account");
        }
        public bool InsertAccount(string name, string displayname , int type)
        {
            string query = string.Format("Insert Account ( UserName, DisplayName, Type)Values (N'{0}', N'{1}', {2})", name, displayname, type);
            int result = DataProvider.Instance.ExecuteNonQuery(query);
            return result > 0;
        }
        public bool UpdateAccount(string name, string displayname, int type)
        {
            string query = string.Format("Update Account Set DisplayName =N'{0}' , Type = {1}  where UserName = N'{2}'", displayname, type,name);
            int result = DataProvider.Instance.ExecuteNonQuery(query);
            return result > 0;
        }
        public bool DeleteAccount(string name )
        { 
            string query = string.Format("Delete Account where UserName = N'{0}'", name);
            int result = DataProvider.Instance.ExecuteNonQuery(query);
            return result > 0;
        }
        public DataTable SearchAccoutByDisplayName(string name)
        {
            string query = string.Format("select UserName , DisplayName ,Type from Account where DisplayName like N'%{0}%'", name);
             return DataProvider.Instance.ExecuteQuery(query);
            
        }
    }
}
