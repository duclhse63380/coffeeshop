﻿using CoffeeShop.DAO;
using CoffeeShop.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class frmAccountProfile : Form
    {
        private Account loginAccount;

        public Account LoginAccount
        {
            get { return loginAccount; }
            set { loginAccount = value; ChangeAccount(loginAccount); }
        }
        public frmAccountProfile(Account account)
        {
            InitializeComponent();
            this.LoginAccount = account;
        }
        void ChangeAccount(Account account)
        {
            txtUserName.Text = loginAccount.UserName;
            txtDisplayName.Text = loginAccount.DisplayName;


        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
           
        }
        void UpdateProfile()
        {
            string displayName = txtDisplayName.Text;
            string password = txtPassword.Text;
            string newPass = txtNewPassword.Text;
            string reEnter = txtReEnterPassword.Text;
            string userName = txtUserName.Text;
            if (newPass != reEnter)
            {
                MessageBox.Show("Please Re-Eter the Password to Connect with New Password! ");
            }
            else
            {
                if (AccountDAO.Instance.UpdateAccount(userName, displayName, password, newPass))
                {
                    MessageBox.Show("Update Successfull !");

                }
                else
                {
                    MessageBox.Show("Please Enter the Password !");
                   
                }
            }
        }
        
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateProfile();
        }

        private void btnLongExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
 
}
